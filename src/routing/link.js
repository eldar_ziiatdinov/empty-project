/**
 * Класс который обеспечивает привязку <a></a> и контекста роутинга
 */
export class Link {

    constructor({routerContext, pathTo, label, classNames}) {
        this._label = label;
        this._pathTo = pathTo;
        this._routerContext = routerContext;
        this._classNames = classNames;
        this._link = null;
        this._render();
    }

    _render() {
        this._link = document.createElement('a');
        this._link.href = this._pathTo;
        this._link.appendChild(document.createTextNode(this._label));
        this._link.className = this._classNames;
        this._link.addEventListener('click', this._routerContext.handleClick);
    }

    get element() {
        return this._link;
    }
}

const handleClick = (routes) => (e) => { //пример использования замыкания
    e.preventDefault();
    window.history.pushState({}, null, e.target.href);
    routes.forEach(route => route.route());
};

/**
 * Класс который и выполняет роутинг
 * также предоставляет необходимый контекст для ссылок
 */
export class Router {

    constructor({routes}) {
        this._routes = routes;
        this._context = {handleClick: handleClick((this._routes))};
        this._init();
    }

    _init() {
        window.addEventListener('load', () => {
            this._routes.forEach(route => route.route());
        });
        window.addEventListener('popstate', () => {
            this._routes.forEach(route => route.route());
        });
    }

    get context() {
        return this._context;
    }
}

/**
 * Mock(Заглушка) рендеринга основного контента страницы
 * @param text - строка
 * @returns text node
 */
export const createMockContentElement = ({text}) => document.createTextNode(text);

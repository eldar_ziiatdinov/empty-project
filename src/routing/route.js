/**
 * Класс - привязка роута с компонентом(элементом)
 * отвечает за добавление элемента в контайенр если указаный роут(path) совбадает с location
 */
export class Route {

    constructor({pathTo, container, component}) {
        this._pathTo = pathTo;
        this._component = component;
        this._container = container;
    }

    route() {
        if (location.pathname.startsWith(this._pathTo)) {
            this._clearContainer();
            this._container.appendChild(this._component);
        }
    }

    _clearContainer() {
        while (this._container.firstChild){
            this._container.removeChild(this._container.firstChild);
        }
    }
}

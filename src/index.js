import './index.scss';
import {Router, Route, Link, createMockContentElement} from "./routing";

//контейнеры для ссылок и самого сайта
const menuContainer = document.querySelector('.header');
const contentContainer = document.querySelector('.content');

//создание контента
const aboutComponent = createMockContentElement({text: 'Страница About'}); //тут заглушки страничек
const editorComponent = createMockContentElement({text: 'Страница Html-editor'});
const AboutRoute = new Route({pathTo: '/about', container: contentContainer, component: aboutComponent});
const EditorRoute = new Route({pathTo: '/editor', container: contentContainer, component: editorComponent});

//создание роутера
const router = new Router({routes: [AboutRoute, EditorRoute]});

//создание ссылок
const aboutLink = new Link({routerContext: router.context, pathTo: '/about', label: 'About', classNames: 'link'});
const editorLink = new Link({routerContext: router.context, pathTo: '/editor', label: 'Html-editor', classNames: 'link'});
menuContainer.appendChild(aboutLink.element);
menuContainer.appendChild(editorLink.element);

/*
на самом деле механизм router и route можно улучшать и улучшать, но базовый принцип работы роутинга и window.history изложен
    например что можно улучшить:
        -при клике несколько раз подрят по одной и той же ссылке не сохранять в историю записи
        -можно как-то объединить или изменить route и router чтобы можно было добавлять элементы, а не только добавлять либо первый элемент, либо второй.
         тут напрашивается механизм route который может добавлять элементы в контейнер как просто добалять элементы так и добавление по условию.
        -так же можно реализовать механизм redirect(например если мы вобьем руками в урле /блаблабла переходил на главную страницу или 404)

*/
